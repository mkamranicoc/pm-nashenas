import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import { Dashboard, Login, Message, SignUp } from "./components";
import authService from "./services/auth.service";
import logo from "./assets/logo.png";
import spy from "./assets/spy.png";
function App() {
  const RequireAuth = ({ children }) => {
    if (!authService().isLoggedIn()) {
      return <Navigate replace to="/login" />;
    }
    return <>{children}</>;
  };
  return (
    <BrowserRouter>
      <div className="bg-orange-600 w-screen pt-20 flex flex-col min-h-screen  text-right justify-center items-center font-sans font-medium pb-20">
        <h2 className="mb-5 font-bold text-2xl flex items-center">
          <img src={spy} className="w-10 h-10" />
          برنامه ی پیام ناشناس{" "}
        </h2>
        <Routes>
          <Route path="" element={<SignUp />}></Route>
          <Route path="/login" element={<Login />}></Route>
          <Route path="/message/:userId" element={<Message />}></Route>
          <Route
            path="/dashboard/:userId"
            element={
              <RequireAuth>
                <Dashboard />
              </RequireAuth>
            }
          ></Route>
        </Routes>
        <div className=" md:absolute right-5 bottom-5 rounded  mt-32 text-xs font-bold  bg-white p-2 hover:shadow-lg hover:opacity-90 cursor-pointer">
          <a
            href="https://dotenx.com/"
            target={"_blank"}
            className={"flex items-center text-rose-600"}
          >
            <img src={logo} className="w-5 h-5 mr-2 shadow border" />
            made with Dotenx
          </a>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
