import { useNavigate, useParams } from "react-router-dom";
import React, { useEffect, useState } from "react";
import messageService from "../services/message.service";
import authService from "../services/auth.service";
import { ToastContainer, toast } from "react-toastify";
import axios from "axios";
import { BiMailSend } from "react-icons/bi";

export function Message() {
  const { userId } = useParams();
  const [accountID, setAccountID] = useState("");
  const navigate = useNavigate();
  const [message, setMessage] = useState("");
  const getUserId = async () => {
    const res = await axios.get("https://api.dotenx.com/profile", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${authService().getToken()}`,
      },
    });
    setAccountID(res.data.tp_account_id);
  };
  useEffect(() => {
    if (authService().getToken()) {
      getUserId();
    }
  }, []);

  const handleChange = (e) => {
    setMessage(e.target.value);
  };
  useEffect(() => {
    if (!authService().isLoggedIn()) authService().authorize();
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await messageService().sendMessage(userId, message);
      toast(".پیام ارسال شد");
      setMessage("");
    } catch (error) {
      toast(error.message);
    }
  };
  return (
    <div className="flex flex-col items-center font-bold">
      <div className="absolute top-10 right-10">
        {!authService().isLoggedIn() ? (
          <div>
            <span
              onClick={() => navigate("/login")}
              className="ml-4 cursor-pointer hover:text-slate-800 transition-colors "
            >
              ورود
            </span>
            <span
              onClick={() => navigate("/")}
              className=" cursor-pointer hover:text-slate-800 transition-colors"
            >
              ثبت نام
            </span>
          </div>
        ) : (
          <div>
            <span
              onClick={() => navigate("/dashboard/" + accountID)}
              className="mr-4 cursor-pointer hover:text-slate-800 transition-colors"
            >
              داشبورد
            </span>
            <span
              onClick={() => {
                authService().logout();
                navigate("/login");
              }}
              className="mr-4 text-white cursor-pointer hover:text-slate-800 transition-colors"
            >
              خروج
            </span>
          </div>
        )}
      </div>
      <form
        onSubmit={handleSubmit}
        className=" lg:min-w-[400px] rounded flex space-y-5 flex-col p-3 px-4 shadow-2xl text-slate-900 bg-white"
      >
        <h1 className="text-2xl">ارسال پیام ناشناس</h1>
        <hr />
        <textarea
          rows="4"
          onChange={handleChange}
          value={message}
          className="bg-slate-300 rounded p-1 text-slate-700 text-right"
          type="textarea"
          placeholder="...پیام خود را بنویسید"
          name="message"
          required
        ></textarea>
        <div className=" text-white ">
          <button
            type="submit"
            className="bg-blue-400 hover:bg-blue-500 float-right flex items-center"
          >
            <BiMailSend className="  mr-1" />
            ارسال
          </button>
        </div>
      </form>
      <div
        className="w-fit rounded p-2  text-center mt-3 cursor-pointer underline underline-offset-2 hover:text-slate-800 transition-colors "
        onClick={() => navigate("/")}
      >
        دریافت لینک
      </div>

      <ToastContainer
        position="top-right"
        autoClose={1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
