import { React, useState } from "react";
import { useNavigate } from "react-router-dom";
import authService from "../services/auth.service";
import { ToastContainer, toast } from "react-toastify";

export function SignUp() {
  const navigate = useNavigate();
  const [form, setForm] = useState({
    fullname: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await authService().register(form.fullname, form.email, form.password);
      toast(".ثبت نام انجام شد");
      navigate("/login");
    } catch (error) {
      toast(error.message);
    }
  };
  return (
    <div>
      <form
        onSubmit={handleSubmit}
        className="bg-white rounded flex space-y-5 flex-col p-5 shadow-lg text-slate-900 "
      >
        <h2 className="text-3xl font-semibold text-center">ثبت نام</h2>
        <div className="flex items-start justify-center text-sm">
          <div
            onClick={() => navigate("/login")}
            className="text-purple-700 hover:text-purple-400 cursor-pointer mr-1 "
          >
            ورود
          </div>
          ثبت نام کرده اید؟
        </div>
        <hr />
        <input
          className="bg-slate-300 rounded p-3 text-slate-700  text-right"
          onChange={handleChange}
          type="text"
          placeholder="نام"
          name="fullname"
          required
        ></input>
        <input
          className="bg-slate-300 rounded p-3 text-slate-700 text-right"
          onChange={handleChange}
          type="email"
          placeholder="ایمیل"
          name="email"
          required
        ></input>
        <input
          className="bg-slate-300 rounded p-3 text-slate-700 text-right"
          onChange={handleChange}
          type="password"
          placeholder="رمز عبور"
          name="password"
          required
        ></input>
        <button
          type="submit"
          className="bg-orange-600 hover:bg-orange-500 shadow shadow-amber-900 border-none  hover:shadow-none"
        >
          ثبت نام
        </button>
      </form>
      <ToastContainer
        position="top-right"
        autoClose={1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
