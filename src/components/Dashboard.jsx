import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import authService from "../services/auth.service";
import messageService from "../services/message.service";
import { BsInboxFill } from "react-icons/bs";
import { RiFileCopy2Line } from "react-icons/ri";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export function Dashboard() {
  const navigate = useNavigate();
  const { userId } = useParams();
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    fetchMessages();
  }, []);

  const fetchMessages = async () => {
    const res = await axios.post(
      "https://api.dotenx.com/database/query/select/project/9CNor1B5iapfiZks/table/messages",
      {
        columns: [],
        filters: {
          filterSet: [
            {
              key: "userid",
              operator: "=",
              value: userId,
            },
          ],
          conjunction: "and",
        },
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${authService().getToken()}`,
        },
      }
    );
    setMessages(res.data);
    res.data.map((m) => {
      if (!m.seen) seenMessage(m.id);
    });
  };
  const seenMessage = async (id) => {
    try {
      await messageService().checkMessage(id);
    } catch (error) {
      toast(error.message);
    }
  };
  const handleDeleteMessage = async (id) => {
    try {
      await messageService().deleteMessage(id);
      toast(".پیام حذف شد");
      fetchMessages();
    } catch (error) {
      toast(error.message);
    }
  };

  return (
    <div className="w-full flex flex-col justify-center items-center">
      <h3 className="mb-4 text-sm">
        <span className="block mb-1 mr-1 md:inline">:لینک شما</span>
        <span
          onClick={() => {
            navigator.clipboard.writeText(
              `https://pm-nashenas.netlify.app/message/${userId}`
            );
          }}
          className="cursor-pointer text-center mx-1 bg-slate-100 rounded p-1 text-slate-800 active:bg-slate-500 flex items-center  text-xs"
        >
          pm-nashenas.netlify.app/message/{userId}
          <RiFileCopy2Line className="block md:inline" />
        </span>
      </h3>
      <div className="bg-white rounded flex space-y-5 flex-col p-3 px-4 shadow-lg text-slate-900 w-3/4 md:w-1/2">
        <div className="absolute top-10 right-10">
          <span
            onClick={() => {
              authService().logout();
              navigate("/login");
            }}
            className="mr-4 text-white cursor-pointer hover:text-slate-800 transition-colors"
          >
            خروج
          </span>
        </div>
        <h3 className="text-3xl flex items-center justify-center">
          <BsInboxFill className="pt-2  ml-2 " /> صندوق پیام ها
        </h3>
        <hr />
        <div className="flex flex-col  ">
          {messages ? (
            messages.map((m) => (
              <div
                className={`bg-slate-200 rounded p-2 my-2 ${
                  m.seen ? "order-2 bg-slate-100" : "order-1 shadow"
                }`}
              >
                <div
                  className={`float-right text-xs -mt-5 text-green-700  ${
                    m.seen ? "hidden" : "inline"
                  }`}
                >
                  جدید
                </div>
                <p className="font-medium">{m.message}</p>
                <button
                  type="button"
                  className={`float-right text-xs rounded p-1 text-white bg-red-700 ml-1 active:bg-red-500 mt-2`}
                  onClick={() => handleDeleteMessage(m.id)}
                >
                  حذف
                </button>
              </div>
            ))
          ) : (
            <div className="text-center">پیامی ندارید</div>
          )}
        </div>
      </div>
      <ToastContainer
        position="top-right"
        autoClose={1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
