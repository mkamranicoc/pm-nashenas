import axios from "axios";
import { React, useState } from "react";
import { useNavigate } from "react-router-dom";
import authService from "../services/auth.service";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
export function Login() {
  const navigate = useNavigate();
  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const getUserId = async () => {
    const res = await axios.get("https://api.dotenx.com/profile", {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${authService().getToken()}`,
      },
    });
    navigate("/dashboard/" + res.data.tp_account_id);
  };
  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await authService().login(form.email, form.password);
      toast("خوش آمدید");
      getUserId();
    } catch (error) {
      toast(error.message);
    }
  };
  return (
    <div>
      <form
        onSubmit={handleSubmit}
        className="bg-white rounded flex space-y-5 flex-col p-5 shadow-lg text-slate-900"
      >
        <h1 className="text-3xl font-semibold  ">وارد حساب خود شوید</h1>
        <div className="flex items-center justify-center text-sm">
          <div
            onClick={() => navigate("/")}
            className="text-purple-700 hover:text-purple-400 cursor-pointer mr-1"
          >
            ثبت نام
          </div>
          اکانت ندارید؟
        </div>
        <hr />
        <input
          onChange={handleChange}
          className="bg-slate-300 rounded p-3 text-slate-700 text-right"
          type="email"
          placeholder="ایمیل"
          name="email"
          required
        ></input>
        <input
          onChange={handleChange}
          className="bg-slate-300 rounded p-3 text-slate-700 text-right"
          type="password"
          placeholder="رمز عبور"
          name="password"
          required
        ></input>
        <button
          type="submit"
          className="bg-blue-400 hover:bg-blue-500 shadow shadow-sky-900 hover:shadow-none"
        >
          ورود
        </button>
      </form>

      <ToastContainer
        position="top-right"
        autoClose={1000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}
