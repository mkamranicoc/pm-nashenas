import axios from "axios";

function authService() {
  const register = async (fullname, email, password) => {
    try {
      await axios.post(
        "https://api.dotenx.com/user/management/project/9CNor1B5iapfiZks/register",
        {
          fullname,
          email,
          password,
        }
      );
    } catch (error) {
      throw new Error(`Failed to sign up: ${error.message}`);
    }
  };

  const login = async (email, password) => {
    try {
      const response = await axios.post(
        "https://api.dotenx.com/user/management/project/9CNor1B5iapfiZks/login",
        {
          email,
          password,
        }
      );

      localStorage.setItem("accessToken", response.data.accessToken);
      localStorage.setItem("expirationTime", response.data.expirationTime);
    } catch (error) {
      throw new Error(`Failed to log in: ${error.message}`);
    }
  };

  const logout = () => {
    localStorage.removeItem("accessToken");
    localStorage.removeItem("expirationTime");
  };

  const getToken = () => {
    return localStorage.getItem("accessToken");
  };

  const isLoggedIn = () => {
    return localStorage.getItem("accessToken") ? true : false;
  };
  const authorize = async () => {
    try {
      const response = await axios.post(
        "https://api.dotenx.com/user/management/project/9CNor1B5iapfiZks/login",
        {
          email: "admin123@email.com",
          password: "admin123",
        }
      );
      localStorage.setItem("token", response.data.accessToken);
    } catch (error) {
      throw new Error(`Failed to log in: ${error.message}`);
    }
  };
  return {
    authorize,
    register,
    login,
    logout,
    getToken,
    isLoggedIn,
  };
}

export default authService;
