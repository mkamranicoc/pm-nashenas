import axios from "axios";
import authService from "./auth.service";

function messageService() {
  const sendMessage = async (userId, message) => {
    try {
      await axios.post(
        "https://api.dotenx.com/database/query/insert/project/9CNor1B5iapfiZks/table/messages",
        {
          userId,
          message,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${
              authService().getToken() || localStorage.getItem("token")
            }`,
          },
        }
      );
    } catch (error) {
      throw new Error(`Failed to send message: ${error.message}`);
    }
  };
  const checkMessage = async (id) => {
    try {
      await axios.put(
        "https://api.dotenx.com/database/query/update/project/9CNor1B5iapfiZks/table/messages/row/" +
          id,
        {
          seen: true,
        },
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${authService().getToken()}`,
          },
        }
      );
    } catch (error) {
      throw new Error(`Failed to check message: ${error.message}`);
    }
  };
  const deleteMessage = async (id) => {
    try {
      await axios.delete(
        "https://api.dotenx.com/database/query/delete/project/9CNor1B5iapfiZks/table/messages/row/" +
          id,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${authService().getToken()}`,
          },
        }
      );
    } catch (error) {
      throw new Error(`Failed to delete message: ${error.message}`);
    }
  };

  return {
    sendMessage,
    checkMessage,
    deleteMessage,
  };
}

export default messageService;
